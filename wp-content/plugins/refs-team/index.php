<?php
/*
Plugin Name: Refs Team
Plugin URI: http://refstecnologia.com.br
Description: Team Refs Plugin para WordPress.
Version: 1.0
Author: codepx
Author URI: http://codepixel.com.br
License: GPLv2
Text Domain: team-manager-free
Domain Path: /languages
*/

//define('REFS_TEAM_PLUGIN_PATH', WP_PLUGIN_URL . '/' . plugin_basename( dirname(__FILE__) ) . '/' );
define('REFS_TEAM_PLUGIN_PATH', get_home_url().'/wp-content/plugins/' . plugin_basename( dirname(__FILE__) ) . '/' );

include 'admin/post-type.php';
include 'admin/option-meta-box.php';
include 'shortcode.php';
include 'short_shortcode.php';
include 'map_shortcode.php';


add_action( 'wp_enqueue_scripts', 'wp_action_enqueue_scripts' );
function wp_action_enqueue_scripts()
{
	wp_enqueue_script( 'ajax-team', 	REFS_TEAM_PLUGIN_PATH . 'js/ajax.js', array(), null, true );
	wp_enqueue_script( 'carousel-team', 	REFS_TEAM_PLUGIN_PATH . 'dist/owl.carousel.min.js', array(), null, true );
	//wp_enqueue_script( 'carousel-team', REFS_TEAM_PLUGIN_PATH . 'js/boxSlide/jquery.bxslider.js', array(), null, true );
	//wp_enqueue_style( 'style-carousel-team',  REFS_TEAM_PLUGIN_PATH . 'js/boxSlide/jquery.bxslider.css', array(), null, true );
	wp_enqueue_style( 'style-carousel-team',  REFS_TEAM_PLUGIN_PATH . 'dist/assets/owl.carousel.min.css', array(), null, true );
	wp_enqueue_style( 'theme-carousel-team',  REFS_TEAM_PLUGIN_PATH . 'dist/assets/owl.theme.default.min.css', array(), null, true );
	wp_enqueue_style('icon_refs_team', 	REFS_TEAM_PLUGIN_PATH . 'css/iconsocial.css');
	wp_enqueue_style('style_refs_team', REFS_TEAM_PLUGIN_PATH . 'css/style.css');
}
