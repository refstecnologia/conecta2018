jQuery(document).ready(function($) {
	// fitVids.
	$( '.entry-content' ).fitVids();

	// Responsive wp_video_shortcode().
	//$( '.wp-video-shortcode' ).parent( 'div' ).css( 'width', 'auto' );

	/**
	 * Odin Core shortcodes
	 */

	// Tabs.
	$( '.odin-tabs a' ).click(function(e) {
		e.preventDefault();
		$(this).tab( 'show' );
	});

	// Tooltip.
	$( '.odin-tooltip' ).tooltip();

	/*$('.cols-1-1 .background, .col-1x1').ready(function(){
		var w = $(this).css('width');
		$(this).css('min-height', w);
	});*/


	$('body').find('.cols-1-1 .background, .col-1x1').each(function(){
       	var w = $(this).css('width');
		$(this).css('min-height', w);
	});

	/*$('.datas a').click(function(){
		var id = $(this).attr('href');
		$(id).find('.col-1x1').each(function(){
	       	var w = $(this).width();
	       	window.alert(w);
			$(this).css('min-height', w);
		});
	});*/


	$(window).scroll(function(event){
  		event.preventDefault();

		if(window.innerWidth > 956){
	  		if ($(this).scrollTop() > 200) {
	  			$('.page-header').animate({margin: '10px 0 10px'});
	  			$('.page-header .custom-logo').animate({'max-height': '50px'});
	  			$('#main-navigation').animate({margin: '5px 0'});
	  			$('#header').animate({'height': '75px' }, {complete: function(){ $('#getting-started').addClass('fixed'); } });
	  			$('#wrapper').animate({'padding-top': '75px'});

	  			

	  		}else{
	  			$('.page-header').clearQueue();
	  			$('.page-header .custom-logo').clearQueue();
	  			$('#main-navigation').clearQueue();
	  			$('#header').clearQueue();
				$('#wrapper').clearQueue();
				$('#countdown > div').clearQueue();

	  			$('.page-header').animate({margin: '20px 0 20px'});
	  			$('.page-header .custom-logo').animate({'max-height': '70px'});
	  			$('#main-navigation').animate({margin: '20px 0'});
	  			$('#header').animate({'height': '110px' }, {complete: function(){ $('#getting-started').removeClass('fixed'); }});
	  			$('#wrapper').animate({'padding-top': '110px'});

	  			
	  		}
	  	}else{
	  		if ($(this).scrollTop() > 100) {
	  			$('.social').fadeIn('100');
	  		}else{
	  			$('.social').fadeOut('100');
	  		}
	  	}
  	});

  	
  	$(document).on('hover', '.team-item', function(){
  		$(this).find('.social_icons').fadeToggle('slow');
  	});


  	$('.datas a').click(function(e){
  		e.preventDefault();
  		var dia = $(this).attr('href');
  		$('.datas a').removeClass('active');
  		$(this).addClass('active');
  		$('.dia').removeClass('active');
  		$(dia).addClass('active');


  	});

  	$('.filtros .filtro').click(function(){
  		var filtro = $(this).attr('filtro');
  		if($(this).hasClass('active')){
  			$(this).removeClass('active');
	  		$(filtro).slideUp();
  		}else{
  			$('.filtros .filtro_opcoes').slideUp();
	  		$('.filtros .filtro').removeClass('active');
	  		$(this).addClass('active');
	  		//$('.filtro_opcoes').hide();
	  		$(filtro).slideDown();
	  	}
  	});

  	$(document).on('click', '.filtrados a', function(){
	    $('.rm_'+ $(this).attr('item')).remove();
	});

	$('.filtro_reset').click(function(){

		$('.filtro_opcoes').find('input').each(function(){
			$(this).prop('checked', false);
		});
		$('.filtrados').html('');
		$('.item-evento, .item').show();

		if ($.trim($('.filtrados').html()) === ''){ $('.filtro_reset').hide(); }
	});

  	$('.filtro_opcoes.horarios input').click(function(){
  		var checado = 0;

		$('.horario').hide();

  		var value = $(this).val();

  		if( $('.filtrados span').hasClass('rm_'+value) ){
  			$('.filtrados .rm_'+value).remove();
  			if ($.trim($('.filtrados').html()) === ''){ $('.filtro_reset').hide(); }
  		}else{
  			$('.filtrados').append('<span class="rm_' + value + '" >Hora ' + $(this).attr('rotulo') + '<label for="campo_'+value+'" class="remove_filter" item="'+value+'" ></label></span>');
  			$('.filtro_reset').show();
  		}

  		$('.filtro_opcoes.horarios').find('input').each(function(){
		    if($(this).attr('checked') === 'checked'){
		       var item = $(this).val();
		       $('.H_'+item).show();
		       checado += 1;
		    }
		});

		if(checado === 0){
			$('.horario').show();
		}
		
		
  	});

  	$('.filtro_opcoes.salas input').click(function(){
  		var checado = 0;
		$('.item-evento .item').hide();
  		
  		var value = $(this).val();

  		if( $('.filtrados span').hasClass('rm_'+value) ){
  			$('.filtrados .rm_'+value).remove();
  			if ($.trim($('.filtrados').html()) === ''){ $('.filtro_reset').hide(); }
  		}else{
  			$('.filtrados').append('<span class="rm_' + value + '" >Sala ' + $(this).attr('rotulo') + '<label for="campo_'+value+'" class="remove_filter" item="'+value+'" ></label></span>');
  			$('.filtro_reset').show();
  		}
  		
  		$('.filtro_opcoes.salas').find('input').each(function(){
		    if($(this).attr('checked') === 'checked'){
		       var item = $(this).val();
		       $('.S_'+item).show();
		       checado += 1;
		    }
		});
		if(checado === 0){
			$('.item.sala').show();
		}

		limpa_linha();
  	});

  	$('.filtro_opcoes.temas input').click(function(){
  		var checado = 0;
		$('.item-evento .item').hide();
  		
  		var value = $(this).val();

  		if( $('.filtrados span').hasClass('rm_'+value) ){
  			$('.filtrados .rm_'+value).remove();
  			if ($.trim($('.filtrados').html()) === ''){ $('.filtro_reset').hide(); }
  		}else{
  			$('.filtrados').append('<span class="rm_' + value + '" >Tema ' + $(this).attr('rotulo') + '<label for="campo_'+value+'" class="remove_filter" item="'+value+'" ></label></span>');
  			$('.filtro_reset').show();
  		}

  		$('.filtro_opcoes.temas').find('input').each(function(){
		    if($(this).attr('checked') === 'checked'){
		       var item = $(this).val();
		       $('.T_'+item).show();
		       checado += 1;
		    }
		});
		if(checado === 0){
			$('.item.tema').show();
		}

		limpa_linha();
  	});

  	$('.filtro_opcoes.palestrantes input').click(function(){
  		var checado = 0;
		$('.item-evento .item').hide();
  		
  		var value = $(this).val();

  		if( $('.filtrados span').hasClass('rm_'+value) ){
  			$('.filtrados .rm_'+value).remove();
  			if ($.trim($('.filtrados').html()) === ''){ $('.filtro_reset').hide(); }
  		}else{
  			$('.filtrados').append('<span class="rm_' + value + '" >Palestrante ' + $(this).attr('rotulo') + '<label for="campo_'+value+'" class="remove_filter" item="'+value+'" ></label></span>');
  			$('.filtro_reset').show();
  		}

  		$('.filtro_opcoes.palestrantes').find('input').each(function(){
		    if($(this).attr('checked') === 'checked'){
		       var item = $(this).val();
		       $('.P_'+item).show();
		       checado += 1;
		    }
		});
		if(checado === 0){
			$('.item.palestrante').show();
		}

		limpa_linha();
  	});

  	$('.close-filter').click(function(){
  		$('.filtros .mobile').fadeOut();
  	});

  	$('.open-filter').click(function(){
  		$('.filtros .mobile').fadeIn();
  	});

  	function limpa_linha(){
		$('.horario').find('.item-evento').each(function(){
			
			var linha = $(this).attr('linha');

			$(linha).show();

			var vazia = true;
			$(this).find('.item').each(function(){
				var oculta = $(this).attr('style');
				if(oculta !== 'display: none;'){
					vazia = false;
				}
			});

			if(vazia){
				$(linha).hide();
			}
		});
	}


	$(document).on('click', '.midiakit', function(e){
		e.preventDefault();
		var fileName = 'Midiakit_Conecta_2017.pdf';
	    var fileURL = '/wp-content/uploads/'+fileName;
	    window.open(fileURL, '_blank');
	});



	$('.load-more-palestrante button').click(function(){
		var valor = $(this).val();
		var atts  = $(this).attr('atts');
		var obj = jQuery.parseJSON(atts);
		$('.load-more-palestrante .status').html('<i class="fa fa-spinner fa-spin fa-fw"></i> Carregando...');
		$.ajax({
		    type: 'POST',
		    url: '/wp-admin/admin-ajax.php',
		    data: {action:'refs_load_more', init:valor, atts:atts},
		    success: function( response ){
		    	if(response===''){
					$('.load-more-palestrante .status').html('Não existe mais registro para carregar.');
		    		$('.load-more-palestrante button').hide();
		    	}else{
		            $('.team').append(response);
		            $('.load-more-palestrante .status').html('');
		            valor = Number.parseInt(valor)+Number.parseInt(obj.limit);
		            $('.load-more-palestrante button').val(valor);
		        }
		    }
		});
	});

	if( $('body').width() < 768 ){
		

		$('#ptp-130').ready(function(){
			var content = $('#ptp-130').html();
			$('#ptp-130').html('<div class="abas" ></div>');
			$('#ptp-130').append(content);

			var i = 0;
			$('#ptp-130').find('.ptp-item-container .ptp-plan').each(function(){
				var destaque = '';
				if( $('.ptp-col-id-'+i).hasClass('ptp-highlight') ){
					destaque = 'highlight';
				}
				var item = '<div class="bt-aba '+destaque+'" item=".ptp-col-id-'+i+'" >'+$(this).html()+'</div>';
				$('#ptp-130 .abas').append(item);
				i++;
			});

			$('.ptp-item-container .ptp-plan').hide();

			$('.ptp-col').hide();
			$('.ptp-col-id-0').show();

			$('.bt-aba').click(function(){
				$('.bt-aba').removeClass('active');
				$(this).addClass('active');
				var item = $(this).attr('item');
				$('.ptp-col').hide(0, function(){
					$(item).show(0);
				});
				
			});

			$('.abas .bt-aba:nth-child(1)').addClass('active');
		});
	}

	/*$('.player .fluid-width-video-wrapper').addClass('capa');

	$('.player .capa').click(function(){
		$('.player .fluid-width-video-wrapper').removeClass('capa');
		$('.player .fluid-width-video-wrapper iframe').attr('src', $('.fluid-width-video-wrapper iframe').attr('src') + '?autoplay=1');
	});*/

	$('.rolar a, .rolar').click(function (event) {
		//event.preventDefault();
		var idElemento = $(this).attr('href');
		var deslocamento = $(idElemento).offset().top;
		$('html, body').animate({ scrollTop: deslocamento }, 'slow');
  	});

	var c = true;
	$(window).scroll(function(event){
  		event.preventDefault();

  		if ( $('#counter').length ){
	  		var posCounter = $('#counter').offset().top - window.innerHeight;

			if($(this).scrollTop() > posCounter && c){
				c = false;
			  	$('.count').each(function () {
				    $(this).prop('Counter',0).animate({
				        Counter: $(this).text()
				    }, {
				        duration: 4000,
				        easing: 'swing',
				        step: function (now) {
				            $(this).text(Math.ceil(now));
				        }
				    });
				});
		  	}
		}
	  });
/*
	$('#countdown').countdown('2017/08/02 07:00:00').on('update.countdown', function(event) {
	  var $this = $(this).html(event.strftime('<div class="dias">%D<span>Dias</span></div><div class="hora">%H<span>Horas</span></div><div class="min">%M<span>Min.</span></div><div class="seg">%S<span>Seg.</span></div>'));
	});

*/

	if(window.innerWidth < 768){
		$('#carousel-refsContent').owlCarousel();
	}
});


function midiakit(){
	jQuery(document).ready(function($) {
		var interesse = $('select[name=interesse]').val();
		$('#submit-parceiro').val('Baixe o Mídia Kit Conecta Imobi');
		$('#submit-parceiro').attr('interesse', interesse);
		$('#submit-parceiro').addClass('midiakit');
	});
}


