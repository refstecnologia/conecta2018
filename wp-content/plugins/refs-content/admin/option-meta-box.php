<?php

/*----------------------------------------------------------------------
	Add Meta Box 
----------------------------------------------------------------------*/
function refs_content_custom_post_meta_box() {
	add_meta_box(
		'custom_meta_box', // $id
		'Detalhe do membro', // $title
		'refs_content_custom_inner_custom_boxes', // $callback
		'refs_content', // $page
		'normal', // $context
		'high'); // $priority
}
add_action('add_meta_boxes', 'refs_content_custom_post_meta_box');

/*----------------------------------------------------------------------
	Content Options Meta Box 
----------------------------------------------------------------------*/

function refs_content_custom_inner_custom_boxes( $post ) {

	// Use nonce for verification
	wp_nonce_field( plugin_basename( __FILE__ ), 'refs_content_custom_inner_custom_boxes_noncename' );

	?>
	<!-- URL -->

	<p><label for="post_url"><strong><?php _e('URL Link:', 'refs-content-manager');?></strong></label></p>

	<input type="text" name="post_url" id="post_url" class="regular-text code" value="<?php echo get_post_meta($post->ID, 'content_link', true); ?>" />
	<hr class="horizontalRuler"/>


	<!-- Icone -->

	<?php include "icons-meta-box.php"; ?>

	<p><label for="post_icon"><strong><?php _e('Ícone:', 'refs-content-manager');?></strong></label></p>


	<input type="hidden" name="post_icon" id="post_icon" value="<?php echo get_post_meta($post->ID, 'content_icon', true); ?>" />
	<div class="btn-group-icons" role="group" >
	<?php foreach ($icons as $value => $name) { ?>
		<button type="button" value="<?php echo $value; ?>" class="btn btn-default <?php echo get_post_meta($post->ID, 'content_icon', true) == $value ? 'active' : ''; ?>"><i class="fa <?php echo $value; ?>" ></i></button>
	<?php } ?>
	</div>



	<?php
}

/*===============================================
	Save Options Meta Box Function
=================================================*/

function refs_content_custom_inner_custom_boxes_save($post_id)
{
	/*----------------------------------------------------------------------
		Designation
	----------------------------------------------------------------------*/
	if(isset($_POST['post_url'])) {
		update_post_meta($post_id, 'content_link', $_POST['post_url']);
	}

	/*----------------------------------------------------------------------
		Description
	----------------------------------------------------------------------*/
	if(isset($_POST['post_icon'])) {
		update_post_meta($post_id, 'content_icon', $_POST['post_icon']);
	}
	
}

/*----------------------------------------------------------------------
	Save Options Meta Box Action
----------------------------------------------------------------------*/
add_action('save_post', 'refs_content_custom_inner_custom_boxes_save');





/*----------------------------------------------------------------------
	Columns Declaration Function
----------------------------------------------------------------------*/
function refs_content_columns($refs_content_columns){

	$order='asc';

	if($_GET['order']=='asc') {
		$order='desc';
	}

	$refs_content_columns = array(
		"cb" 		=> "<input type=\"checkbox\" />",

		#"thumbnail" => __('Image', 'refs-content-manager'),

		"title" 	=> __('Name', 'refs-content-manager'),

		"categories" => __('Grupos', 'refs-content-manager'),

		//"date" => __('Date', 'refs-content-manager'),
	);

	return $refs_content_columns;

}


/*----------------------------------------------------------------------
	testimonial Value Function
----------------------------------------------------------------------*/
function refs_content_columns_display($refs_content_columns, $post_id){

	global $post;

	$width = (int) 80;
	$height = (int) 80;

	/*if ( 'thumbnail' == $refs_content_columns ) {

		if ( has_post_thumbnail($post_id)) {
			$thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
			$thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );
			echo $thumb;
		}
		else 
		{
			echo __('None');
		}
	}*/


	if ( 'categories' == $refs_content_columns ) {

		$terms = get_the_terms( $post_id , 'refs_content_category');
		$count = count($terms);

		if ( $terms ){

			$i = 0;

			foreach ( $terms as $term ) {
				echo '<a href="'.admin_url( 'edit.php?post_type=refs_content&refs_content_category='.$term->slug ).'">'.$term->name.'</a>';	
				
				if($i+1 != $count) {
					echo " , ";
				}
				$i++;
			}
		}
	}
}


/*----------------------------------------------------------------------
	Add manage_tmls_posts_columns Filter 
----------------------------------------------------------------------*/
add_filter("manage_refs_content_posts_columns", "refs_content_columns");

/*----------------------------------------------------------------------
	Add manage_tmls_posts_custom_column Action
----------------------------------------------------------------------*/
add_action("manage_refs_content_posts_custom_column",  "refs_content_columns_display", 10, 2 );