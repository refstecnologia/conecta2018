<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main div element.
 *
 * @package Odin
 * @since 2.2.0
 */
?>
	<?php echo do_shortcode('[patrocinadores]'); ?>
	</div><!-- #wrapper -->

	<footer id="footer" role="contentinfo">
		<div class="container">
			<p>O Conecta Imobi é um evento realizado pelo portal VivaReal. Copyright &copy; 2014-<?php echo date( 'Y' ); ?> <?php _e( 'All rights reserved', 'odin' ); ?>.</p>
			
				<div class="col-sm-12">
					<h5><a href="https://www.conectaimobi.com.br/politica-de-privacidade/" target="_blank">Acesse nossa Política de Privacidade</a></h5>
				</div>
				<div class="col-sm-6">
					<h5>APROVEITE E CONECTE-SE COM A GENTE:</h5>
					<h4 class="hashtag color-blue" >#CONECTAIMOBI2018</h4>
				</div>
				<div class="col-sm-6 app">
					<h5>BAIXE O APLICATIVO DO CONECTA:</h5>
					<a href="#"><img src="<?php echo get_assets_image_url('button-google-play.png'); ?>" width="120" alt=""></a> &nbsp;
					<a href="#"><img src="<?php echo get_assets_image_url('button-app-store.png'); ?>" width="120" alt=""></a>
				</div>

		</div><!-- .container -->

		<div class="refs-develop-credit">DESENVOLVIDO POR: <a title="Refs Tecnologia - Inovação é o nosso negócio! Conheça-nos clicando aqui." href="http://www.refstecnologia.com.br?utm_source=site-conecta&amp;utm_medium=logo&amp;utm_campaign=conecta" target="_blank" rel="nofollow"><img style="max-height: 22px; vertical-align: middle; display: inline;" src="https://www.conectaimobi.com.br/refs-tecnologia-site-sistemas-logo-branco.png" alt=" "></a></div>

	</footer><!-- #footer -->
	

<!-- Push Notification -->
  <script type="text/javascript">
    (function(p,u,s,h){
        p._pcq=p._pcq||[];
        p._pcq.push(['_currentTime',Date.now()]);
        s=u.createElement('script');
        s.type='text/javascript';
        s.async=true;
        s.src='https://cdn.pushcrew.com/js/84ab5cddc9c643c57d2324b9d51f139c.js';
        h=u.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s,h);
    })(window,document);
</script>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-ML24FQR');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-ML24FQR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "18278794" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=18278794&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->

<!-- begin SnapEngage code -->
<script type="text/javascript">
  (function() {
    var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true;
    se.src = '//storage.googleapis.com/code.snapengage.com/js/b1c23333-590c-4b02-a9ca-77ddb78c8854.js';
    var done = false;
    se.onload = se.onreadystatechange = function() {
      if (!done&&(!this.readyState||this.readyState==='loaded'||this.readyState==='complete')) {
        done = true;
        /* Place your SnapEngage JS API code below */
        /* SnapEngage.allowChatSound(true); Example JS API: Enable sounds for Visitors. */
      }
    };
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(se, s);
  })();
</script>
<!-- end SnapEngage code -->

<!-- Hotjar Tracking Code for http://www.conectaimobi.com.br -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:455848,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>


<script>
$= jQuery;
$(document).ready(function(){
	$("a[href='/ingresso-standard']").attr('onclick', "ga('send', 'event', 'Ingresso Standard', 'Clique', 'Botao');" );
	$("a[href='/ingresso-premium']").attr('onclick', "ga('send', 'event', 'Ingresso Premium', 'Clique', 'Botao');" );
});


</script>

<!--  Home page tag ---->
<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
<script type="text/javascript">
window.criteo_q = window.criteo_q || [];
window.criteo_q.push(
        { event: "setAccount", account: 38743 },
        { event: "setSiteType", type: "d" },
        { event: "viewHome" }
);
</script>

	<?php wp_footer(); ?>
</body>
</html>


