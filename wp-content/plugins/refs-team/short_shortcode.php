<?php

// Add Shortcode
function refs_team_custom_shortcode_ajax( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'group' => '',
			'type' => 'grid',
			'position_title' => 'bottom',
			'position_designation' => 'bottom',
			'coluns' => 4,
			'coluns_mobile' => 2,
			'limit' => 12,
			'start' => 0,
			'tag_title' => 'h2',
			'tag_designation' => 'h4',
			'view_social' => true,
			'view_image' => true,
			'view_title' => true,
			'view_description' => false,
			'view_designation' => true,
			'image_size' => 'full',
			'link_target' => false,
			'carousel_in_mobile' => false,
			'class_button' => 'button',
			'load_more' => false,
			'text_button' => 'Carregar mais',
			'class_developer' => ''
		),
		$atts
	);


	$atts = (object) $atts;
	$class_cols  = 'col-xs-' . floor(12 / $atts->coluns_mobile);
	$class_cols .= ' col-sm-' . floor(12 / $atts->coluns);

	$image_size = explode('x', $atts->image_size);
	if(count($image_size) == 1){
		$image_size = $image_size[0];
	}

	// WP_Query arguments
	$args = array(
		'post_type'              => array( 'refs_team' ),
		'posts_per_page'         => $atts->limit,
		'offset'				 => $atts->start,
		'order'                  => 'ASC',
		'orderby'                => 'menu_order',
		'tax_query'              => array(
			array(
				'taxonomy'         => 'refs_team_category',
				'field'			   => 'slug',
				'terms'            => $atts->group,
			),
		),
		
	);

	// The Query
	$query = new WP_Query( $args );

	// The Loop
	if ( $query->have_posts() ) {


		$c = 0;
		while ( $query->have_posts() ) {
			$query->the_post();
			// do something
			$c += 1;

			switch ($atts->type) {
				case 'line':
					/********* Template LINE **********/

					echo '<div class="tean-item line col-sm-12" >';


					if( has_post_thumbnail() ){
						$thumbnail = wp_get_attachment_image_url( get_post_thumbnail_id(), 'refs_team_image' );
						echo '<figure class="col-sm-2">';
						echo '<img src="'.$thumbnail.'" alt="'.(get_the_title()).'" />';
						echo '</figure>';
					}

					echo '<div class="tean-content col-sm-10" >';

					if($atts->link_target == 'page') echo '<a href="'.get_permalink().'" title="'.get_the_title().'" >';

					if($atts->view_title == 'true'){
						echo "<{$atts->tag_title}>" . get_the_title() . "</{$atts->tag_title}>";
					}else{
						echo '<h3></h3>';
					}
					
					if($atts->view_designation == 'true')
						echo $designation;echo "<{$atts->tag_designation}>" . get_post_meta(get_the_ID(), "client_designation", true) . "</{$atts->tag_designation}>";


					if($atts->link_target == 'page') echo '</a>';

					if($atts->view_social == 'true'){
						echo '<div class="social_icons" >';
						if(get_post_meta(get_the_ID(), 'social_facebook', true)) 	echo '<a href="'.get_post_meta(get_the_ID(), 'social_facebook', true).'" target="_blank" class="icon-facebook" ></a>';
						if(get_post_meta(get_the_ID(), 'social_instagran', true)) 	echo '<a href="'.get_post_meta(get_the_ID(), 'social_instagran', true).'" target="_blank" class="icon-instagram" ></a>';
						if(get_post_meta(get_the_ID(), 'social_linkedin', true)) 	echo '<a href="'.get_post_meta(get_the_ID(), 'social_linkedin', true).'" target="_blank" class="icon-linkedin" ></a>';
						if(get_post_meta(get_the_ID(), 'social_twitter', true)) 	echo '<a href="'.get_post_meta(get_the_ID(), 'social_twitter', true).'" target="_blank" class="icon-twitter" ></a>';
						echo '</div>';
					}

					echo '</div>';

					echo '</div>';


					/********* End LINE **********/

					break;



				case 'grid':

					/********* Template GRID **********/

					echo '<div class="team-item grid '.$class_cols.'" >';

					$title 			= "<{$atts->tag_title}>" . get_the_title() . "</{$atts->tag_title}>";
					$designation 	= "<{$atts->tag_designation}>" . get_post_meta(get_the_ID(), "client_designation", true) . "</{$atts->tag_designation}>";


					if($atts->position_title == 'top' && $atts->view_title == 'true') echo $title;
					if($atts->view_designation == 'true' && $atts->position_designation == 'top') echo $designation;

					if( has_post_thumbnail() ){
						$thumbnail = wp_get_attachment_image_url( get_post_thumbnail_id(), 'refs_team_image' );
						echo '<figure>';
						echo '<img src="'.$thumbnail.'" alt="'.(get_the_title()).'" width="480" height="420" />';

						if($atts->view_social == 'true'){
							echo '<div class="social_icons rows" >';
							if(get_post_meta(get_the_ID(), 'social_facebook', true)) 	echo '<a href="'.get_post_meta(get_the_ID(), 'social_facebook', true).'" target="_blank" class="icon-facebook" ></a>';
							if(get_post_meta(get_the_ID(), 'social_googleplus', true)) 	echo '<a href="'.get_post_meta(get_the_ID(), 'social_googleplus', true).'" target="_blank" class="icon-google-plus" ></a>';
							if(get_post_meta(get_the_ID(), 'social_linkedin', true)) 	echo '<a href="'.get_post_meta(get_the_ID(), 'social_linkedin', true).'" target="_blank" class="icon-linkedin" ></a>';
							if(get_post_meta(get_the_ID(), 'social_twitter', true)) 	echo '<a href="'.get_post_meta(get_the_ID(), 'social_twitter', true).'" target="_blank" class="icon-twitter" ></a>';
							echo '</div>';
						}

						echo '</figure>';
					}

					if($atts->link_target == 'page') echo '<a href="'.get_permalink().'" title="'.get_the_title().'" >';

					if($atts->position_title == 'bottom' && $atts->view_title == 'true') echo $title;
					if($atts->view_designation == 'true' && $atts->position_designation == 'bottom') echo $designation;

					echo '<div class="description rows" >';
					if($atts->view_description == 'true'){
						
						if(get_post_meta(get_the_ID(), 'client_shortdescription', true)){
							echo get_post_meta(get_the_ID(), 'client_shortdescription', true);
						}else{
							the_content();
						}
						
					}
					echo '</div>';

					echo '</div>';


					/********* End GRID **********/

					break;
				
				
			}

		}

	} else {
		return false;
	}

	// Restore original Post Data
	wp_reset_postdata();

}
add_shortcode( 'rfs_team_ajax', 'refs_team_custom_shortcode_ajax' );