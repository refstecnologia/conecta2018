<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */


if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
	include( dirname( __FILE__ ) . '/wp-config-local.php' );
}else{
	// ** Configurações do MySQL - Você pode pegar estas informações
	// com o serviço de hospedagem ** //
	/** O nome do banco de dados do WordPress */
	define( 'DB_NAME', 'refst210_conectaimobi2018' );

	/** Usuário do banco de dados MySQL */
	define( 'DB_USER', 'refst210_hadmin' );

	/** Senha do banco de dados MySQL */
	define( 'DB_PASSWORD', 'z9B3o3G_TsTE' );

	/** Nome do host do MySQL */
	define( 'DB_HOST', 'localhost' );

	/** Charset do banco de dados a ser usado na criação das tabelas. */
	define( 'DB_CHARSET', 'utf8' );

	/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
	define( 'DB_COLLATE', '' );

}

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N8Z]8$ce#A]so{QJOijaRXrfylLCW1K>32)BA&%S=alCXy|iZEJP*dz +RzS|]{0');
define('SECURE_AUTH_KEY',  ' Mus#6`v(1[U.gc.|PGUx0QGyA5{vXO8`<2`s4b8<Eos*m5rwvJ/u5+kQl^q15?o');
define('LOGGED_IN_KEY',    '^Td5X@g[/atyQZ6gDZEJwUndXBH^)C]vzyt RE?NgVoI2q+ K;/n!GoppbI0)zqm');
define('NONCE_KEY',        'J%~^j43_-g_q}*hLfgfl:66,Jo(@b?~4<n`aW@UDY@DA!3,(Amp4g#q4-m`AzS!J');
define('AUTH_SALT',        'A.tlG@!;cG/:hW1:)EQp,dTG/x3O_-9mO96wa?kr7E$T_rrH:I[4lB+H;dG*f=`H');
define('SECURE_AUTH_SALT', '3vp2%cy;k8@V^2[+M49JTNH^iNuo.7kKe6H}H{M| 2{o?&-,AL6OYJ`FTsGcLZ;0');
define('LOGGED_IN_SALT',   'AO9r$j,V,1l|A(l}lsE_%4l!9-8>zfVX#Q7y~oGUZfFEM!Xa GM0~!l7tZcM&jdo');
define('NONCE_SALT',       'HI 8CXn1!6_].@sLLC<v0j*NN$.v]U7YKD[[? ,X<p#I4fIR#S&[|FeB[&;ISivD');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'vr_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once( ABSPATH . 'wp-settings.php' );
