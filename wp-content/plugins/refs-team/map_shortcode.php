<?php
add_action('vc_before_init', 'refs_team_settings');

function refs_team_settings(){
	vc_map( array(
	    "name" => __("Refs Member", "rfs_member"), // add a name
	    "base" => "rfs_team", // bind with our shortcode
	    "class" => "",
	    "icon" => REFS_TEAM_PLUGIN_PATH . 'images/team-icon.jpg',
	    "category" => __( "Content", "my-text-domain"),
	    "params" => array(
	         array(
	            "type" => "textfield", // it will bind a textfield in WP
	            "heading" => __("Grupo", "rfs_member"),
	            "param_name" => "group",
	        ),

	        array(
				'type' => 'dropdown',
				'heading' => __( 'Tipo', 'rfs_member' ),
				'param_name' => 'type',
				'value' => array(
					'Grade' => 'grid',
					'Lista' => 'list',
					'Carrossel' => 'carousel',
				),
				'description' => __( 'Defina o tipo de apresentação.', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Posição do Título', 'rfs_member' ),
				'param_name' => 'position_title',
				'value' => array(
					'Depois da imagem' => 'bottom',
					'Antes da imagem' => 'top',
				),
				'description' => __( '', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Posição da Designação', 'rfs_member' ),
				'param_name' => 'position_designation',
				'value' => array(
					'Depois da imagem' => 'bottom',
					'Antes da imagem' => 'top',
				),
				'description' => __( '', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Número de colunas', 'rfs_member' ),
				'param_name' => 'coluns',
				'value' => array(''=>4,2,3,4,6,12),
				'description' => __( 'Somente para o formato grid', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Número de colunas Responsivo', 'rfs_member' ),
				'param_name' => 'coluns_mobile',
				'value' => array(''=>2,1,2,3),
				'description' => __( 'Somente para o formato grid', 'rfs_member' ),
			),

			array(
				'type' => 'textfield',
				'heading' => __( 'Quantidade de Itens', 'rfs_member' ),
				'param_name' => 'limit',
				'value' => 12,
				'description' => __( '', 'rfs_member' ),
			),

	        array(
				'type' => 'dropdown',
				'heading' => __( 'Tag do Título', 'rfs_member' ),
				'param_name' => 'tag_title',
				'value' => array(
					''	 => 'h2',
					'h1' => 'h1',
					'h2' => 'h2',
					'h3' => 'h3',
					'h4' => 'h4',
					'h5' => 'h5',
					'h6' => 'h6',
					'p' => 'p class="title"',
				),
				'description' => __( '', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Tag da Designação', 'rfs_member' ),
				'param_name' => 'tag_designation',
				'value' => array(
					''	 =>	'h4',
					'h1' => 'h1',
					'h2' => 'h2',
					'h3' => 'h3',
					'h4' => 'h4',
					'h5' => 'h5',
					'h6' => 'h6',
					'p' => 'p class="designation"',
				),
				'description' => __( '', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Exibir redes sociais', 'rfs_member' ),
				'param_name' => 'view_social',
				'value' => array(
					'Sim' => 'true',
					'Não' => 'false',
				),
				'description' => __( '', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Exibir Imagem', 'rfs_member' ),
				'param_name' => 'view_image',
				'value' => array(
					'Sim' => 'true',
					'Não' => 'false',
				),
				'description' => __( '', 'rfs_member' ),
				'std' => 'Sim',
			),

			array(
				'type' => 'textfield',
				'heading' => __( 'Tamanho da imagem', 'rfs_member' ),
				'param_name' => 'image_size',
				'value' => 'full',
				'description' => __( 'Utilize tamanhos de imagens pre-definidos como "thumbnail", "medium", "large", "full" ou defina o tamanho da imagem com o formato largura x altura. Ex.: 300x350.', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Exibir Título', 'rfs_member' ),
				'param_name' => 'view_title',
				'value' => array(
					'Sim' => 'true',
					'Não' => 'false',
				),
				'description' => __( '', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Exibir Descrição', 'rfs_member' ),
				'param_name' => 'view_description',
				'value' => array(
					'Sim' => 'true',
					'Não' => 'false',
				),
				'description' => __( '', 'rfs_member' ),
			),


			array(
				'type' => 'dropdown',
				'heading' => __( 'Exibir Designação', 'rfs_member' ),
				'param_name' => 'view_designation',
				'value' => array(
					'Sim' => 'true',
					'Não' => 'false',
				),
				'description' => __( '', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Exibir carousel no mobile', 'rfs_member' ),
				'param_name' => 'carousel_in_mobile',
				'value' => array(
					'Não'	=> 'false',
					'Sim'	=> 'true'
					
				),
				'description' => __( '', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Ação ao Clicar', 'rfs_member' ),
				'param_name' => 'link_target',
				'value' => array(
					'Nada' => 'false',
					'Direcionar à página' => 'page',
					'Abrir no Box' => 'box',
					'Nada' => 'false',
				),
				'description' => __( '', 'rfs_member' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Exibir botão carregar mais?', 'rfs_member' ),
				'param_name' => 'load_more',
				'value' => array(
					'Não' => 'false',
					'Sim' => 'true'
					
				),
				'description' => __( '', 'rfs_member' ),
			),

			array(
	            "type" => "textfield", // it will bind a textfield in WP
	            "heading" => __("Texto do botão Carregar mais", "rfs_member"),
	            "param_name" => "text_button",
	            "value" => 'Carregar mais'
	        ),

	        array(
	            "type" => "textfield", // it will bind a textfield in WP
	            "heading" => __("Classe CSS", "rfs_member"),
	            "param_name" => "class_developer",
	        ),

	        array(
	            "type" => "textfield", // it will bind a textfield in WP
	            "heading" => __("Classe Botão Carregar mais", "rfs_member"),
	            "param_name" => "class_button",
	            "value" => 'button'
	        )
	    )
	) );
}