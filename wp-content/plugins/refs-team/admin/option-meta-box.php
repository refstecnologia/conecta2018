<?php

/*----------------------------------------------------------------------
	Add Meta Box 
----------------------------------------------------------------------*/
function refs_team_manager_custom_post_meta_box() {
	add_meta_box(
		'custom_meta_box', // $id
		'Detalhe do membro', // $title
		'refs_team_manager_custom_inner_custom_boxes', // $callback
		'refs_team', // $page
		'normal', // $context
		'high'); // $priority
}
add_action('add_meta_boxes', 'refs_team_manager_custom_post_meta_box');

/*----------------------------------------------------------------------
	Content Options Meta Box 
----------------------------------------------------------------------*/

function refs_team_manager_custom_inner_custom_boxes( $post ) {

	// Use nonce for verification
	wp_nonce_field( plugin_basename( __FILE__ ), 'refs_team_manager_custom_inner_custom_boxes_noncename' );

	?>


	<!-- Designation -->

	<p><label for="post_title_designation"><strong><?php _e('Designação:', 'refs-team-manager');?></strong></label></p>

	<input type="text" name="post_title_designation" id="post_title_designation" class="regular-text code" value="<?php echo get_post_meta($post->ID, 'client_designation', true); ?>" />
	<hr class="horizontalRuler"/>


	<!-- Description -->

	<p><label for="short_description_input"><strong><?php _e('Breve descrição (Max 140 caracteres):', 'refs-team-manager');?></strong></label></p>

	<textarea name="short_description_input" id="short_description_input" class="regular-text code" cols="30" rows="4" maxlength="140"><?php echo get_post_meta($post->ID, 'client_shortdescription', true); ?></textarea>
	<hr class="horizontalRuler"/>

	<!-- Address  -->

	<p><label for="client_address_input"><strong><?php _e('Endereço:', 'refs-team-manager');?></strong></label></p>

	<input type="text" name="client_address_input" placeholder="Winston Salem, NC" id="client_address_input" class="regular-text code" value="<?php echo get_post_meta($post->ID, 'company_address', true); ?>" />
	<hr class="horizontalRuler"/>

	<!-- Contact Number -->

	<p><label for="contact_number_input"><strong><?php _e('Número de telefone:', 'refs-team-manager');?></strong></label></p>

	<input type="text" name="contact_number_input" placeholder="21 xxxx-xxxx" id="contact_number_input" class="regular-text code" value="<?php echo get_post_meta($post->ID, 'contact_number', true); ?>" />
	<hr class="horizontalRuler"/>

	<!-- Contact Email -->

	<p><label for="contact_email_input"><strong><?php _e('Email:', 'refs-team-manager');?></strong></label></p>

	<input type="text" name="contact_email_input" placeholder="email@exapmle.com" id="contact_email_input" class="regular-text code" value="<?php echo get_post_meta($post->ID, 'contact_email', true); ?>" />
	<hr class="horizontalRuler"/>

	<!-- Info Section -->

	<p style="color:red"><label for="company_intro_box"><strong><?php _e('Redes Sociais', 'refs-team-manager');?></strong></label></p>
	<hr class="horizontalRuler"/>

	<!-- Facebook -->

	<p><label for="facebook_social_input"><strong><?php _e('Facebook:', 'refs-team-manager');?></strong></label></p>

	<input type="text" name="facebook_social_input" placeholder="http://exapmle.com/username" id="facebook_social_input" class="regular-text code" value="<?php echo get_post_meta($post->ID, 'social_facebook', true); ?>" />
	<hr class="horizontalRuler"/>

	<!-- Twitter -->

	<p><label for="twitter_social_input"><strong><?php _e('Twitter:', 'refs-team-manager');?></strong></label></p>

	<input type="text" name="twitter_social_input" placeholder="http://exapmle.com/username" id="twitter_social_input" class="regular-text code" value="<?php echo get_post_meta($post->ID, 'social_twitter', true); ?>" />
	<hr class="horizontalRuler"/>

	<!-- Linkedin -->

	<p><label for="linkedin_social_input"><strong><?php _e('Linkedin:', 'refs-team-manager');?></strong></label></p>

	<input type="text" name="linkedin_social_input" placeholder="http://exapmle.com/username" id="linkedin_social_input" class="regular-text code" value="<?php echo get_post_meta($post->ID, 'social_linkedin', true); ?>" />		
	<hr class="horizontalRuler"/>

	<!-- Google plus -->

	<p><label for="googleplus_social_input"><strong><?php _e('Google Plus:', 'refs-team-manager');?></strong></label></p>

	<input type="text" name="googleplus_social_input" placeholder="http://exapmle.com/username" id="googleplus_social_input" class="regular-text code" value="<?php echo get_post_meta($post->ID, 'social_googleplus', true); ?>" />		
	<hr class="horizontalRuler"/>

	<?php
}

/*===============================================
	Save Options Meta Box Function
=================================================*/

function refs_team_manager_custom_inner_custom_boxes_save($post_id)
{
	/*----------------------------------------------------------------------
		Designation
	----------------------------------------------------------------------*/
	if(isset($_POST['post_title_designation'])) {
		update_post_meta($post_id, 'client_designation', $_POST['post_title_designation']);
	}

	/*----------------------------------------------------------------------
		Description
	----------------------------------------------------------------------*/
	if(isset($_POST['short_description_input'])) {
		update_post_meta($post_id, 'client_shortdescription', $_POST['short_description_input']);
	}

	/*----------------------------------------------------------------------
		address
	----------------------------------------------------------------------*/
	if(isset($_POST['client_address_input'])) {
		update_post_meta($post_id, 'company_address', $_POST['client_address_input']);
	}

	/*----------------------------------------------------------------------
		contact number
	----------------------------------------------------------------------*/
	if(isset($_POST['contact_number_input'])) {
		update_post_meta($post_id, 'contact_number', $_POST['contact_number_input']);
	}

	/*----------------------------------------------------------------------
		Email
	----------------------------------------------------------------------*/
	if(isset($_POST['contact_email_input'])) {
		update_post_meta($post_id, 'contact_email', $_POST['contact_email_input']);
	}

	/*----------------------------------------------------------------------
		Facebook
	----------------------------------------------------------------------*/
	if(isset($_POST['facebook_social_input'])) {
		update_post_meta($post_id, 'social_facebook', $_POST['facebook_social_input']);
	}

	/*----------------------------------------------------------------------
		Twitter
	----------------------------------------------------------------------*/
	if(isset($_POST['twitter_social_input'])) {
		update_post_meta($post_id, 'social_twitter', $_POST['twitter_social_input']);
	}

	/*----------------------------------------------------------------------
		Linkedin Plus
	----------------------------------------------------------------------*/
	if(isset($_POST['linkedin_social_input'])) {
		update_post_meta($post_id, 'social_linkedin', $_POST['linkedin_social_input']);
	}

	/*----------------------------------------------------------------------
		Google Plus
	----------------------------------------------------------------------*/
	if(isset($_POST['googleplus_social_input'])) {
		update_post_meta($post_id, 'social_googleplus', $_POST['googleplus_social_input']);
	}		
}

/*----------------------------------------------------------------------
	Save Options Meta Box Action
----------------------------------------------------------------------*/
add_action('save_post', 'refs_team_manager_custom_inner_custom_boxes_save');





/*----------------------------------------------------------------------
	Columns Declaration Function
----------------------------------------------------------------------*/
function refs_team_manager_columns($refs_team_manager_columns){

	$order='asc';

	if($_GET['order']=='asc') {
		$order='desc';
	}

	$refs_team_manager_columns = array(
		"cb" 		=> "<input type=\"checkbox\" />",

		"thumbnail" => __('Image', 'refs-team-manager'),

		"title" 	=> __('Name', 'refs-team-manager'),

		"client_shortdescription" => __('Descrição', 'refs-team-manager'),

		"client_designation" => __('Designação', 'refs-team-manager'),

		"ktstcategories" => __('Grupos', 'refs-team-manager'),

		//"date" => __('Date', 'refs-team-manager'),
	);

	return $refs_team_manager_columns;

}


/*----------------------------------------------------------------------
	testimonial Value Function
----------------------------------------------------------------------*/
function refs_team_manager_columns_display($refs_team_manager_columns, $post_id){

	global $post;

	$width = (int) 80;
	$height = (int) 80;

	if ( 'thumbnail' == $refs_team_manager_columns ) {

		if ( has_post_thumbnail($post_id)) {
			$thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
			$thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );
			echo $thumb;
		}
		else 
		{
			echo __('None');
		}
	}

	if ( 'client_designation' == $refs_team_manager_columns ) {
		echo get_post_meta($post_id, 'client_designation', true);
	}
	if ( 'client_shortdescription' == $refs_team_manager_columns ) {
		echo get_post_meta($post_id, 'client_shortdescription', true);
	}

	if ( 'ktstcategories' == $refs_team_manager_columns ) {

		$terms = get_the_terms( $post_id , 'refs_team_category');
		$count = count($terms);

		if ( $terms ){

			$i = 0;

			foreach ( $terms as $term ) {
				echo '<a href="'.admin_url( 'edit.php?post_type=refs_team&refs_team_category='.$term->slug ).'">'.$term->name.'</a>';	
				
				if($i+1 != $count) {
					echo " , ";
				}
				$i++;
			}
		}
	}
}


/*----------------------------------------------------------------------
	Add manage_tmls_posts_columns Filter 
----------------------------------------------------------------------*/
add_filter("manage_refs_team_posts_columns", "refs_team_manager_columns");

/*----------------------------------------------------------------------
	Add manage_tmls_posts_custom_column Action
----------------------------------------------------------------------*/
add_action("manage_refs_team_posts_custom_column",  "refs_team_manager_columns_display", 10, 2 );