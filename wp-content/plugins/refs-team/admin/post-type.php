<?php

if ( ! defined( 'ABSPATH' ) )

	die("Can't load this file directly");	



/*===================================================================
	Register Custom Post Function
=====================================================================*/

function refs_team_manager_custom_post_type(){
		$labels = array(
			'name'                  => _x( 'Membros', 'Post Type General Name', 'refs-team-manager' ),
			'singular_name'         => _x( 'Membros', 'Post Type Singular Name', 'refs-team-manager' ),
			'menu_name'             => __( 'Membros', 'refs-team-manager' ),
			'name_admin_bar'        => __( 'Membros', 'refs-team-manager' ),
			'parent_item_colon'     => __( 'Parent Item:', 'refs-team-manager' ),
			'all_items'             => __( 'Membros', 'refs-team-manager' ),
			'add_new_item'          => __( 'Novo Membro', 'refs-team-manager' ),
			'add_new'               => __( 'Novo Membro', 'refs-team-manager' ),
			'new_item'              => __( 'New Membro', 'refs-team-manager' ),
			'edit_item'             => __( 'Edit Membro', 'refs-team-manager' ),
			'update_item'           => __( 'Update Membro', 'refs-team-manager' ),
			'view_item'             => __( 'View Membro', 'refs-team-manager' ),
			'search_items'          => __( 'Search Membro', 'refs-team-manager' ),
			'not_found'             => __( 'Sem registro', 'refs-team-manager' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'refs-team-manager' ),
			'items_list'            => __( 'Items list', 'refs-team-manager' ),
			'items_list_navigation' => __( 'Items list navigation', 'refs-team-manager' ),
			'filter_items_list'     => __( 'Filter items list', 'refs-team-manager' ),
		);
		$args = array(
			'label'                 => __( 'Post Type', 'refs-team-manager' ),
			'description'           => __( 'Post Type Description', 'refs-team-manager' ),
			'labels'                => $labels,
			'supports'              =>  array( 'title', 'editor', 'thumbnail',),
			'hierarchical'          => false,
			'public'                => true,
			'menu_icon' 			=> 'dashicons-groups',
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,		
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'refs_team', $args );

	}
	// end custom post type
add_action('init', 'refs_team_manager_custom_post_type');






function refs_team_manager_custom_post_taxonomies_reg() {
		  $labels = array(
			'name'              => _x( 'Grupos', 'taxonomy general name' ),
			'singular_name'     => _x( 'Grupo', 'taxonomy singular name' ),
			'search_items'      => __( 'Encontrar Grupos' ),
			'all_items'         => __( 'Todos os Grupos' ),
			'parent_item'       => __( 'Parent Group' ),
			'parent_item_colon' => __( 'Parent Group:' ),
			'edit_item'         => __( 'Editar Grupo' ), 
			'update_item'       => __( 'Atualizar Grupo' ),
			'add_new_item'      => __( 'Novo Grupo' ),
			'new_item_name'     => __( 'Novo Grupo' ),
			'menu_name'         => __( 'Grupos' ),
		  );
		  $args = array(
			'labels' => $labels,
			'hierarchical' => true,
		  );
		  register_taxonomy( 'refs_team_category', 'refs_team', $args );
	}
	add_action( 'init', 'refs_team_manager_custom_post_taxonomies_reg', 0 );



function refs_team_manager_admin_enter_title( $input ) {
		global $post_type;

		if ( 'refs_team' == $post_type )
			return __( 'Digite o nome do novo membro', 'refs-team-manager' );

		return $input;
	}
	add_filter( 'enter_title_here', 'refs_team_manager_admin_enter_title' );




function refs_team_manager_custom_post_help($content){
		global $post_type,$post;
		if ($post_type == 'refs_team') {
			if(!has_post_thumbnail( $post->ID )){
			   $content .= '<p>'.__('Por favor, adicione imagem com largura e altura mínima de 500px','refs-team-manager').'</p>';
			}
		}
		return $content;
	}
	add_filter('admin_post_thumbnail_html','refs_team_manager_custom_post_help');





function refs_team_manager_custom_post_updated_messages( $messages ) {
		global $post, $post_id;
		$messages['refs_team'] = array(
			1 => __('Membro atualizado.', 'refs-team-manager'),
			2 => $messages['post'][2],
			3 => $messages['post'][3],
			4 => __('Membro atualizado.', 'refs-team-manager'),
			5 => isset($_GET['revision']) ? sprintf( __('Team Showcase restored to revision from %s', 'refs-team-manager'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => __('Membro publicado.', 'refs-team-manager'),
			7 => __('Membro salvo.', 'refs-team-manager'),
			8 => __('Membro enviado.', 'refs-team-manager'),
			9 => sprintf( __('Membro programado para: <strong>%1$s</strong>.', 'refs-team-manager'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) )),
			10 => __('Team Showcase draft updated.', 'refs-team-manager'),
		);
		return $messages;
	}
	add_filter( 'post_updated_messages', 'refs_team_manager_custom_post_updated_messages' );