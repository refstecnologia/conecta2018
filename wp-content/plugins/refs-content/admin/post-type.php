<?php

if ( ! defined( 'ABSPATH' ) )

	die("Can't load this file directly");	



/*===================================================================
	Register Custom Post Function
=====================================================================*/

function refs_content_custom_post_type(){
		$labels = array(
			'name'                  => _x( 'Conteúdo', 'Post Type General Name', 'refs-content-manager' ),
			'singular_name'         => _x( 'Conteúdo', 'Post Type Singular Name', 'refs-content-manager' ),
			'menu_name'             => __( 'Conteúdos', 'refs-content-manager' ),
			'name_admin_bar'        => __( 'Conteúdos', 'refs-content-manager' ),
			'parent_item_colon'     => __( 'Parent Item:', 'refs-content-manager' ),
			'all_items'             => __( 'Conteúdos', 'refs-content-manager' ),
			'add_new_item'          => __( 'Novo Conteúdo', 'refs-content-manager' ),
			'add_new'               => __( 'Novo Conteúdo', 'refs-content-manager' ),
			'new_item'              => __( 'New Content', 'refs-content-manager' ),
			'edit_item'             => __( 'Edit Content', 'refs-content-manager' ),
			'update_item'           => __( 'Update Content', 'refs-content-manager' ),
			'view_item'             => __( 'View Content', 'refs-content-manager' ),
			'search_items'          => __( 'Search Content', 'refs-content-manager' ),
			'not_found'             => __( 'Sem registro', 'refs-content-manager' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'refs-content-manager' ),
			'items_list'            => __( 'Items list', 'refs-content-manager' ),
			'items_list_navigation' => __( 'Items list navigation', 'refs-content-manager' ),
			'filter_items_list'     => __( 'Filter items list', 'refs-content-manager' ),
		);
		$args = array(
			'label'                 => __( 'Post Type', 'refs-content-manager' ),
			'description'           => __( 'Post Type Description', 'refs-content-manager' ),
			'labels'                => $labels,
			'supports'              =>  array( 'title', 'editor', 'thumbnail',),
			'hierarchical'          => false,
			'public'                => true,
			'menu_icon' 			=> 'dashicons-layout',
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,		
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'refs_content', $args );

	}
	// end custom post type
add_action('init', 'refs_content_custom_post_type');






function refs_content_custom_post_taxonomies_reg() {
		  $labels = array(
			'name'              => _x( 'Grupos', 'taxonomy general name' ),
			'singular_name'     => _x( 'Grupo', 'taxonomy singular name' ),
			'search_items'      => __( 'Encontrar Grupos' ),
			'all_items'         => __( 'Todos os Grupos' ),
			'parent_item'       => __( 'Parent Group' ),
			'parent_item_colon' => __( 'Parent Group:' ),
			'edit_item'         => __( 'Editar Grupo' ), 
			'update_item'       => __( 'Atualizar Grupo' ),
			'add_new_item'      => __( 'Novo Grupo' ),
			'new_item_name'     => __( 'Novo Grupo' ),
			'menu_name'         => __( 'Grupos' ),
		  );
		  $args = array(
			'labels' => $labels,
			'hierarchical' => true,
		  );
		  register_taxonomy( 'refs_content_category', 'refs_content', $args );
	}
	add_action( 'init', 'refs_content_custom_post_taxonomies_reg', 0 );



function refs_content_manager_admin_enter_title( $input ) {
		global $post_type;

		if ( 'refs_content' == $post_type )
			return __( 'Título do Elemento', 'refs-content-manager' );

		return $input;
	}
	add_filter( 'enter_title_here', 'refs_content_manager_admin_enter_title' );




function refs_content_custom_post_help($content){
		global $post_type,$post;
		if ($post_type == 'refs_content') {
			if(!has_post_thumbnail( $post->ID )){
			   $content .= '<p>'.__('Por favor, adicione imagem com largura e altura mínima de 500px','refs-content-manager').'</p>';
			}
		}
		return $content;
	}
	add_filter('admin_post_thumbnail_html','refs_content_custom_post_help');





function refs_content_custom_post_updated_messages( $messages ) {
		global $post, $post_id;
		$messages['refs_content'] = array(
			1 => __('Content atualizado.', 'refs-content-manager'),
			2 => $messages['post'][2],
			3 => $messages['post'][3],
			4 => __('Content atualizado.', 'refs-content-manager'),
			5 => isset($_GET['revision']) ? sprintf( __('Team Showcase restored to revision from %s', 'refs-content-manager'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6 => __('Content publicado.', 'refs-content-manager'),
			7 => __('Content salvo.', 'refs-content-manager'),
			8 => __('Content enviado.', 'refs-content-manager'),
			9 => sprintf( __('Content programado para: <strong>%1$s</strong>.', 'refs-content-manager'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) )),
			10 => __('Team Showcase draft updated.', 'refs-content-manager'),
		);
		return $messages;
	}
	add_filter( 'post_updated_messages', 'refs_content_custom_post_updated_messages' );