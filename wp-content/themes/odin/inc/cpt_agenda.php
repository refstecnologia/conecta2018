<?php
// Register Custom Post Type
function agenda_conecta() {

	$labels = array(
		'name'                  => _x( 'Eventos', 'Post Type General Name', 'agenda_conecta' ),
		'singular_name'         => _x( 'Evento', 'Post Type Singular Name', 'agenda_conecta' ),
		'menu_name'             => __( 'Agenda', 'agenda_conecta' ),
		'name_admin_bar'        => __( 'Agenda', 'agenda_conecta' ),
		'archives'              => __( 'Item Archives', 'agenda_conecta' ),
		'attributes'            => __( 'Item Attributes', 'agenda_conecta' ),
		'parent_item_colon'     => __( 'Parent Item:', 'agenda_conecta' ),
		'all_items'             => __( 'Todos os Eventos', 'agenda_conecta' ),
		'add_new_item'          => __( 'Adicionar novo evento', 'agenda_conecta' ),
		'add_new'               => __( 'Novo Evento', 'agenda_conecta' ),
		'new_item'              => __( 'New Item', 'agenda_conecta' ),
		'edit_item'             => __( 'Edit Item', 'agenda_conecta' ),
		'update_item'           => __( 'Update Item', 'agenda_conecta' ),
		'view_item'             => __( 'View Item', 'agenda_conecta' ),
		'view_items'            => __( 'View Items', 'agenda_conecta' ),
		'search_items'          => __( 'Search Item', 'agenda_conecta' ),
		'not_found'             => __( 'Nenhum evento cadastrado', 'agenda_conecta' ),
		'not_found_in_trash'    => __( 'Nenhum evento econtrado na lixeira', 'agenda_conecta' ),
		'featured_image'        => __( 'Featured Image', 'agenda_conecta' ),
		'set_featured_image'    => __( 'Set featured image', 'agenda_conecta' ),
		'remove_featured_image' => __( 'Remove featured image', 'agenda_conecta' ),
		'use_featured_image'    => __( 'Use as featured image', 'agenda_conecta' ),
		'insert_into_item'      => __( 'Insert into item', 'agenda_conecta' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'agenda_conecta' ),
		'items_list'            => __( 'Items list', 'agenda_conecta' ),
		'items_list_navigation' => __( 'Items list navigation', 'agenda_conecta' ),
		'filter_items_list'     => __( 'Filter items list', 'agenda_conecta' ),
	);
	$args = array(
		'label'                 => __( 'Evento', 'agenda_conecta' ),
		'description'           => __( 'Post Type Description', 'agenda_conecta' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-calendar-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'agenda_conecta', $args );

}
add_action( 'init', 'agenda_conecta', 0 );



// Register Custom Taxonomy
function custom_taxonomy_agenda_sala() {

	$labels = array(
		'name'                       => _x( 'Salas', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Sala', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Salas', 'text_domain' ),
		'all_items'                  => __( 'Todos as salas', 'text_domain' ),
		'new_item_name'              => __( 'Nova sala', 'text_domain' ),

	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'agenda_sala', array( 'agenda_conecta' ), $args );

}
add_action( 'init', 'custom_taxonomy_agenda_sala', 0 );



// Register Custom Taxonomy
function custom_taxonomy_agenda_tema() {

	$labels = array(
		'name'                       => _x( 'Temas', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Tema', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Temas', 'text_domain' ),
		'all_items'                  => __( 'Todos os temas', 'text_domain' ),
		'new_item_name'              => __( 'Novo Tema', 'text_domain' ),

	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'agenda_tema', array( 'agenda_conecta' ), $args );

}
add_action( 'init', 'custom_taxonomy_agenda_tema', 0 );




// Register Custom Taxonomy
function custom_taxonomy_agenda_horario() {

	$labels = array(
		'name'                       => _x( 'Horários', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Horário', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Horários', 'text_domain' ),
		'all_items'                  => __( 'Todos os horários', 'text_domain' ),
		'new_item_name'              => __( 'Novo Horário', 'text_domain' ),
		'add_new_item'               => __( 'Adicionar novo horário', 'text_domain' ),

	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'agenda_horario', array( 'agenda_conecta' ), $args );

}
add_action( 'init', 'custom_taxonomy_agenda_horario', 0 );




// Add Shortcode
function custom_shortcode_agenda_conecta() {


	$taxonomies=get_taxonomies( array( 'name' => 'agenda_sala' ) ); 
	if  ($taxonomies) {
	  $tax_rotules['salas'] = 'Por Sala';
	  foreach ($taxonomies as $taxonomy ) {
	    $terms = get_terms($taxonomy);
	        foreach ( $terms as $term) {
	        	$tax['salas'][$term->slug] = $term->name;	       
	         }
	    }
	}

	$taxonomies=get_taxonomies( array( 'name' => 'agenda_tema' ) ); 
	if  ($taxonomies) {
	  $tax_rotules['temas'] = 'Por Tema';
	  foreach ($taxonomies as $taxonomy ) {
	    $terms = get_terms($taxonomy);
	        foreach ( $terms as $term) {
	        	$tax['temas'][$term->slug] = $term->name;	       
	         }
	    }
	}

	$taxonomies=get_taxonomies( array( 'name' => 'agenda_horario' ) ); 
	if  ($taxonomies) {
	  $tax_rotules['horarios'] = 'Por Horário';
	  foreach ($taxonomies as $taxonomy ) {
	    $terms = get_terms($taxonomy);
	        foreach ( $terms as $term) {
	        	$tax['horarios'][$term->slug] = $term->name;	       
	         }
	    }
	}

	/*$query = new WP_Query( array('post_type'=>'refs_team') );
	// The Loop
	if ( $query->have_posts() ) {
		$tax_rotules['palestrantes'] = 'Por Palestrante';
		while ( $query->have_posts() ) {
			$query->the_post();
			$tax['palestrantes'][get_the_ID()] = get_the_title();
		}
	}*/

	ksort($tax['horarios']);

	/*echo '<pre>';
	print_r($tax);
	echo "</pre>";*/

		$args = array(
			'post_type' => 'agenda_conecta'
		);


		// The Query
		$query = new WP_Query( $args );

		// The Loop
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();

				$post = get_post();

				$ID = get_the_ID();

				$hora = get_the_terms( $ID, 'agenda_horario' );
				$tema = get_the_terms( $ID, 'agenda_tema' );
				$sala = get_the_terms( $ID, 'agenda_sala' );
				$i = $hora[0]->slug;
				$d = get_field('data_evento');


				$palestrante = get_field('palestrante');

				if(count($palestrante) == 1){
					$palestrante = $palestrante[0];

					if(get_field('imagem_alternativa', $palestrante->ID)){
						$_post[$d][$i][$ID]['imagem'] = wp_get_attachment_image_url( get_field('imagem_alternativa', $palestrante->ID), 'Capa Agenda' );
					}else{
						$_post[$d][$i][$ID]['imagem'] = get_the_post_thumbnail_url($palestrante->ID, 'Capa Agenda');
					}
					$_post[$d][$i][$ID]['nome_palestrante'] = $palestrante->post_title;
					if( get_post_meta($palestrante->ID, "client_designation", true) != '' ){
						$_post[$d][$i][$ID]['nome_palestrante'] .= '<span>'.get_post_meta($palestrante->ID, "client_designation", true).'</span>';
					}
					$_post[$d][$i][$ID]['class'][]=$palestrante->ID?'palestrante P_'.$palestrante->ID:'';
				}else{

					foreach ($palestrante as $key => $value) {
						$_post[$d][$i][$ID]['palestrante'][$key]['imagem'] 			= get_the_post_thumbnail_url($value->ID, array(200,200));
						$_post[$d][$i][$ID]['palestrante'][$key]['nome_palestrante'] = $value->post_title;
						if( get_post_meta($value->ID, "client_designation", true) != '' ){
							$_post[$d][$i][$ID]['palestrante'][$key]['designacao'] .= get_post_meta($value->ID, "client_designation", true);
							$_post[$d][$i][$ID]['class'][]=$value->ID?'palestrante P_'.$value->ID:'';
						}
					}
				}

				
				
				// do something
				$_post[$d][$i][$ID]['titulo'] 		= get_the_title();
				$_post[$d][$i][$ID]['descricao'] 	= get_field( 'descript' );
				$_post[$d][$i][$ID]['destaque'] 	= get_field( 'evento_destaque' );
				$_post[$d][$i][$ID]['horario'] 		= $hora[0]->name;
				$_post[$d][$i][$ID]['sala'] 		= $sala[0]->name?'<span class="sala" >'.$sala[0]->name.'</span>':'';
				$_post[$d][$i][$ID]['tema'] 		= $tema[0]->name?'<span class="tema" >'.$tema[0]->name.'</span>':'';
				$_post[$d][$i][$ID]['class'][]		= $sala[0]->slug?'sala S_'.$sala[0]->slug:'';
				
				$_classes_tema = 'tema ';
				if ($tema){
					foreach ( $tema as $tema_item ) {
						$_classes_tema .= ' T_'.$tema_item->slug;
					}
				}
				
				// $_post[$d][$i][$ID]['class'][]		= $tema[0]->slug?'tema T_'.$tema[0]->slug:'';
				$_post[$d][$i][$ID]['class'][]		= $_classes_tema;

			}
		} else {
			// no posts found
		}


		// Restore original Post Data
		wp_reset_postdata();

		
		echo '<div class="agenda" >';

		$dias = array('Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado');
		
		$_datas_agenda = '';
		$_datas_agenda .= '<div class="datas" >';
		$_datas_agenda .=  '<h3>Clique abaixo na data desejada</h3>';

		ksort($_post);
		$class_active = 'active';

		foreach ($_post as $dia => $eventos){
			$y = substr($dia, 0, 4);
			$m = substr($dia, 4, 2);
			$d = substr($dia, -2);

			$_datas_agenda .=  '<a href="#D_'.$dia.'" class="'.$class_active.'" >';
			$_datas_agenda .=  '<span>'.$dias[date("w", mktime(0, 0, 0, $m, $d, $y))] . " | </span><strong>$d.$m</strong>";
			$_datas_agenda .=  '</a>';

			$class_active = '';
		}

		$_datas_agenda .=  '</div>'; #Fechamento div DATA

		echo $_datas_agenda;

		echo '<div class="filtros" >';
		echo '<div class="mobile" >';
		echo '<h3 class="title visible-xs">Agenda</h3> <div class="close-filter" ></div>';
		echo '<h4>Use o filtro abaixo para facilitar a sua busca</h4>';

		foreach ($tax_rotules as $key => $value) {
			echo '<a href="#'.$key.'" filtro=".'.$key.'" class="filtro" >'.$value.'</a>';
		}

		foreach ($tax as $key => $value) {
			echo '<div class="filtro_opcoes '.$key.'" >';
			foreach ($value as $slug_tax => $nome_tax) {
				echo '<label><input type="checkbox" name="'.$slug_tax.'" id="campo_'.$slug_tax.'" value="'.$slug_tax.'" rotulo="'.$nome_tax.'" /> '.$nome_tax.'</label>';
			}
			echo '</div>';
		}

		echo '</div>'; #mobile

		echo '<div class="open-filter visible-xs" >Faça a filtragem de sua busca</div>';

		echo '<div class="filtrados" ></div>';
		echo '<a class="filtro_reset" >Resetar filtragem</a>';

		echo '</div>';




		$class_active = 'active';

		foreach ($_post as $dia => $eventos) {
			echo '<div id="D_'.$dia.'" class="dia '.$class_active.'" >';

			$class_active = '';

			ksort($eventos);

			foreach ($eventos as $horario => $eventos_dia) {
				echo '<div class="horario H_'.$horario.'" >';
				
				echo '
				<div class="item-evento d'.$dia.'_h'.$horario.'" linha=".d'.$dia.'_h'.$horario.'">
					<div class="col-sm-2 col-xs-3 evento-hora">
						'.str_replace(array(':', '00'), array('h', ''), $tax['horarios'][$horario]).'
					</div>';

				echo '<div class="col-sm-10 col-xs-9 evento-content">';

					foreach ($eventos_dia as $key => $value) {
						$value = (object) $value;

						$classes = implode(' ', $value->class);

						echo '<div class="item '.($value->destaque?'destaque ':'').$classes.'" >';
						
						if(!count($value->palestrante)){

							if($value->imagem != '' ) echo '
								<figure class="hidden-xs '.($value->destaque?'col-sm-3':'col-sm-2').' col-1x1" style="background-image: url('.$value->imagem.');">
									<!--<img src="' . $value->imagem . '" alt="">-->
								</figure>';

							echo '<div class="'.($value->destaque?'col-sm-9':'col-sm-10').'">';
									if($value->titulo != '' ) echo '<h2 '.(!$value->nome_palestrante?'class="titulo-agenda"':'').'>' . $value->titulo . '</h2>';
									if($value->nome_palestrante != '' ) echo '<h3>' . $value->nome_palestrante . '</h3>';
									if($value->tema != '' or $value->sala != '' ) echo '<h4>' . $value->tema . ' ' . $value->sala . '</h4>';
									/*if($value->descricao != '' ) echo '<p>' . $value->descricao . '</p>';*/
							echo '</div>';

						}else{
							
							echo '<div class="col-sm-5 col-xs-12">';
									if($value->titulo != '' ) echo '<h2>' . $value->titulo . '</h2>';
									if($value->tema != '' or $value->sala != '' ) echo '<h4>' . $value->tema . ' ' . $value->sala . '</h4>';
									if($value->descricao != '' ) echo '<p>' . $value->descricao . '</p>';
							echo '</div>';

							echo '<div class="col-sm-7 col-xs-12" >';
							foreach ($value->palestrante as $p) {
								echo '<div class="col-sm-6 col-xs-12 item-palestrante">';
								if($p['imagem'] != '' ){
									echo '<img src="' . $p['imagem'] . '" class="hidden-xs avatar" alt="">';
								}
								echo '<h3>' . $p['nome_palestrante'] . '</h3>';
								echo '<h4>' . $p['designacao'] . '</h4>';
								echo '</div>';
							}
							echo '</div>';

						}

							
						echo '</div>';
					}

				echo '</div>';

				echo '</div>';

				echo '</div>';
			}
			echo '</div>';
		}
		echo $_datas_agenda;
		echo '</div>';

		

}
add_shortcode( 'agenda_conecta', 'custom_shortcode_agenda_conecta' );