<?php
/*
Plugin Name: Refs Content
Plugin URI: http://refstecnologia.com.br
Description: Content Refs Plugin para WordPress.
Version: 1.0
Author: codepx
Author URI: http://codepixel.com.br
License: GPLv2
Text Domain: content-free
Domain Path: /languages
*/

//define('REFS_CONTENT_PLUGIN_PATH', WP_PLUGIN_URL . '/' . plugin_basename( dirname(__FILE__) ) . '/' );
define('REFS_CONTENT_PLUGIN_PATH', 'https://www.conectaimobi.com.br/wp-content/plugins/' . plugin_basename( dirname(__FILE__) ) . '/' );

include 'admin/post-type.php';
include 'admin/option-meta-box.php';
include 'shortcode.php';
include 'map_shortcode.php';

add_action( 'after_setup_theme', 'refs_content_configuracoes' );
function refs_content_configuracoes()
{
    add_image_size( 'refs_content_mini', 200, 200, true );
    add_image_size( 'refs_content_image', 500, 500, true );
}


add_action( 'wp_enqueue_scripts', 'wp_action_enqueue_scripts_content' );
function wp_action_enqueue_scripts_content()
{
	wp_enqueue_style('style_refs_content', REFS_CONTENT_PLUGIN_PATH.'css/style.css');
}


add_action( 'admin_enqueue_scripts', 'admin_action_enqueue_scripts_content' );
function admin_action_enqueue_scripts_content(){
	wp_enqueue_style('admin_style_refs_content', REFS_CONTENT_PLUGIN_PATH.'admin/style.css');
	wp_enqueue_script('jquery_script_refs_content', REFS_CONTENT_PLUGIN_PATH.'js/script.js' );
}
