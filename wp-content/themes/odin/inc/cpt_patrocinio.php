<?php

// Register Custom Post Type
function post_type_patrocinio() {

	$labels = array(
		'name'                  => 'Patrocinadores',
		'singular_name'         => 'Patrocinadores',
		'menu_name'             => 'Patrocinadores',
	);
	$args = array(
		'label'                 => 'Patrocinadores',
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-businessman',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'patrocinadores', $args );


	$labels = array(
		'name'                       => _x( 'Classificação', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Classificação', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Classificação', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true
	);
	register_taxonomy( 'tipo_patrocinio', array( 'patrocinadores' ), $args );

}
add_action( 'init', 'post_type_patrocinio', 0 );





// Add term page
function tipo_patrocinio_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="term_meta[coluns]" >Colunas</label>
		<select name="term_meta[coluns]" id="term_meta[coluns]" >
			<option value="1">12</option>
			<option value="2">6</option>
			<option value="3">4</option>
			<option value="4">3</option>
			<option value="6">2</option>
			<option value="12">1</option>
		</select>
		<p class="description"><?php _e( 'Enter a value for this field','pippin' ); ?></p>
	</div>
<?php
}
add_action( 'tipo_patrocinio_add_form_fields', 'tipo_patrocinio_taxonomy_add_new_meta_field', 10, 2 );

// Edit term page
function tipo_patrocinio_taxonomy_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "tipo_patrocinio_$t_id" ); ?>
	
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[coluns]" >Colunas</label></th>
		<td>
			<select name="term_meta[coluns]" id="term_meta[coluns]" >
				<option value="1" <?php echo ( $term_meta['coluns'] == 1 ) ? 'selected' : ''; ?> >12</option>
				<option value="2" <?php echo ( $term_meta['coluns'] == 2 ) ? 'selected' : ''; ?> >6</option>
				<option value="3" <?php echo ( $term_meta['coluns'] == 3 ) ? 'selected' : ''; ?> >4</option>
				<option value="4" <?php echo ( $term_meta['coluns'] == 4 ) ? 'selected' : ''; ?> >3</option>
				<option value="6" <?php echo ( $term_meta['coluns'] == 6 ) ? 'selected' : ''; ?> >2</option>
				<option value="12" <?php echo ( $term_meta['coluns'] == 12 ) ? 'selected' : ''; ?> >1</option>
			</select>
		</td>
	</tr>


<?php
}
add_action( 'tipo_patrocinio_edit_form_fields', 'tipo_patrocinio_taxonomy_edit_meta_field', 10, 2 );


// Save extra taxonomy fields callback function.
function save_taxonomy_tipo_patrocinio( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "tipo_patrocinio_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "tipo_patrocinio_$t_id", $term_meta );
	}
}  
add_action( 'edited_tipo_patrocinio', 'save_taxonomy_tipo_patrocinio', 10, 2 );  
add_action( 'create_tipo_patrocinio', 'save_taxonomy_tipo_patrocinio', 10, 2 );



// Add Shortcode
function patrocinio_shortcode() {

	// WP_Term_Query arguments
	$args = array(
		'taxonomy'               => array( 'tipo_patrocinio' ),
		//'order'                  => 'ASC',
		//'orderby'                => 'term_order',
	);

	// The Term Query
	$term_query = new WP_Term_Query( $args );

	foreach ($term_query->terms as $term) {
		$terms[$term->term_order] = $term;
	}
	ksort($terms);

	// The Loop
	if ( ! empty( $term_query ) && ! is_wp_error( $term_query ) ) {
		echo '<div class="patrocinadores" >';

		echo '<h2 class="titulo" >Patrocinadores</h2>';


		foreach ($terms as $term) {

			// WP_Query arguments
			$args = array(
				'post_type'              => array( 'patrocinadores' ),
				'tax_query' => array(
					array(
						'taxonomy' => 'tipo_patrocinio',
						'field'    => 'slug',
						'terms'    => $term->slug,
					),
				),
			);
			// The Query
			$query = new WP_Query( $args );

			// The Loop
			if ( $query->have_posts() ) {
				echo '<div class="linha '.$term->slug.'" >';
				echo '<h4 class="patrocinador-title col-sm-12 ">'.$term->name.'</h4>';

				$col = get_option('tipo_patrocinio_'.$term->term_id);
				$col = ($col['coluns'])?$col['coluns']:3;

				while ( $query->have_posts() ) {
					$query->the_post();
					if( has_post_thumbnail(get_the_ID()) ){
						echo '<div class="item col-sm-'.$col.'" >';
						the_post_thumbnail( get_the_ID(), 'medium' );
						echo '</div>';
					}
				}
				echo '</div>';
			} else {
				// no posts found
			}

			// Restore original Post Data
			wp_reset_postdata();
		}


		echo '<div class="linha col-sm-12" >';
		echo '<h4>Quer ser um patrocinador ou parceiro?</h4>';
		echo '<a class="button" href="/patrocinio" >Quero saber mais</a>';
		echo '</div>';

		echo '</div>';

	} else {
		// no terms found
	}

}
add_shortcode( 'patrocinadores', 'patrocinio_shortcode' );