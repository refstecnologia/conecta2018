<?php
add_action('vc_before_init', 'refs_content_settings');

function refs_content_settings(){
	vc_map( array(
	    "name" => __("Refs Content", "rfs_content"), // add a name
	    "base" => "rfs_content", // bind with our shortcode
	    "class" => "",
	    "icon" => REFS_CONTENT_PLUGIN_PATH . 'images/icon.png',
	    "category" => __( "Content", "my-text-domain"),
	    "params" => array(
	         array(
	            "type" => "textfield", // it will bind a textfield in WP
	            "heading" => __("Grupo", "rfs_content"),
	            "param_name" => "group",
	        ),

	        array(
				'type' => 'dropdown',
				'heading' => __( 'Tipo', 'rfs_content' ),
				'param_name' => 'type',
				'value' => array(
					'Grade' => 'grid',
					'Lista' => 'list',
				),
				'description' => __( 'Defina o tipo de apresentação.', 'rfs_content' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Número de colunas', 'rfs_content' ),
				'param_name' => 'coluns',
				'value' => array(''=>3,2,3,4,6,12),
				'description' => __( 'Somente para o formato grid', 'rfs_content' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Número de colunas Responsivo', 'rfs_content' ),
				'param_name' => 'coluns_mobile',
				'value' => array(''=>1,1,2,3),
				'description' => __( 'Somente para o formato grid', 'rfs_content' ),
			),

			array(
				'type' => 'textfield',
				'heading' => __( 'Quantidade de Itens', 'rfs_content' ),
				'param_name' => 'limit',
				'value' => 12,
				'description' => __( '', 'rfs_content' ),
			),

	        array(
				'type' => 'dropdown',
				'heading' => __( 'Tag do Título', 'rfs_content' ),
				'param_name' => 'tag_title',
				'value' => array(
					''	 => 'h2',
					'h1' => 'h1',
					'h2' => 'h2',
					'h3' => 'h3',
					'h4' => 'h4',
					'h5' => 'h5',
					'h6' => 'h6',
					'p' => 'p class="title"',
				),
				'description' => __( '', 'rfs_content' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Abrir link em' ),
				'param_name' => 'link_target',
				'value' => array(
					'Janela atual' => '_self',
					'Nova Janela' => '_blank',
				),
				'description' => __( '', 'rfs_content' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Borda do ícone/Imagem' ),
				'param_name' => 'style_icon',
				'value' => array(
					'Circular' => 'circle',
					'Bordas arredondadas' => 'rounded',
					'Sem arredondamento' => 'square',
				),
				'description' => __( '', 'rfs_content' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Tamanho do Ícone' ),
				'param_name' => 'icon_size',
				'value' => array(
					'Mínimo' => 'xs',
					'Pequeno' => 'sm',
					'Médio' => 'md',
					'Largo' => 'lg',
					'Grande' => 'big',
				),
				'description' => __( '', 'rfs_content' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Sombra no ícone/imagem', 'rfs_content' ),
				'param_name' => 'icon_shadow',
				'value' => array(
					'Sim' => 'true',
					'Não' => 'false',
				),
				'description' => __( '', 'rfs_content' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Título em caixa alta', 'rfs_content' ),
				'param_name' => 'title_uppercase',
				'value' => array(
					'Sim' => 'true',
					'Não' => 'false',
				),
				'description' => __( '', 'rfs_content' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Alinhamento do título', 'rfs_content' ),
				'param_name' => 'align_title',
				'value' => array(
					'Centro' => 'center',
					'Esquerdo' => 'left',
					'Direito' => 'right',
				),
				'description' => __( '', 'rfs_content' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => __( 'Alinhamento do título', 'rfs_content' ),
				'param_name' => 'align_text',
				'value' => array(
					'Esquerdo' => 'left',
					'Centro' => 'center',
					'Direito' => 'right',
					'Justificado' => 'justify',
				),
				'description' => __( '', 'rfs_content' ),
			),

			array(
				'type' => 'colorpicker',
				'heading' => __( 'Cor de fundo do ícone', 'rfs_content' ),
				'param_name' => 'background_icon',
				'value' => '#ffffff',
				'description' => __( '', 'rfs_content' ),
			),

			array(
				'type' => 'colorpicker',
				'heading' => __( 'Cor do ícone', 'rfs_content' ),
				'param_name' => 'color_icon',
				'value' => '#000000',
				'description' => __( '', 'rfs_content' ),
			),

			array(
				'type' => 'colorpicker',
				'heading' => __( 'Cor do título', 'rfs_content' ),
				'param_name' => 'color_title',
				'value' => '#000000',
				'description' => __( '', 'rfs_content' ),
			),

			array(
				'type' => 'colorpicker',
				'heading' => __( 'Cor do texto', 'rfs_content' ),
				'param_name' => 'color_text',
				'value' => '#000000',
				'description' => __( '', 'rfs_content' ),
			),

	        array(
	            "type" => "textfield", // it will bind a textfield in WP
	            "heading" => __("Classe CSS", "rfs_content"),
	            "param_name" => "class_developer",
	        )
	    )
	) );
}