<?php

// Add Shortcode
function refs_content_custom_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'group' => '',
			'type' => 'grid',
			'coluns' => 3,
			'coluns_mobile' => 2,
			'limit' => 12,
			'tag_title' => 'h2',
			'link_target' => '_self',
			'style_icon' => 'circle',
			'icon_size' => 'sm',
			'icon_shadow' => true,
			'background_icon' => '#ffffff',
			'color_icon' => '#000000',
			'color_title' => '#000000',
			'color_text' => '#000000',
			'title_uppercase' => true,
			'align_title' => 'center',
			'align_text' => 'center',
			'carousel_in_mobile' => true,
			'class_developer' => ''
		),
		$atts
	);

	$atts = (object) $atts;


	$class_icon 	= $atts->style_icon.' '.$atts->icon_size.' '.($atts->icon_shadow?'shadow':'').'" style="background: '.$atts->background_icon.'; color: '.$atts->color_icon;
	$class_title 	= $atts->align_title.' '.($atts->style_icon?'uppercase':'').'" style="color: '.$atts->color_title;
	$class_text 	= $atts->align_text.'" style="color: '.$atts->color_text;

	
	$class_cols  = 'col-xs-' . floor(12 / $atts->coluns_mobile);
	$class_cols .= ' col-sm-' . floor(12 / $atts->coluns);

	// WP_Query arguments
	$args = array(
		'post_type'              => array( 'refs_content' ),
		'posts_per_page'         => '12',
		'order'                  => 'ASC',
		'orderby'                => 'menu_order',
	);

	if( $atts->group != '' ){
		$args['tax_query'] = array(
								array(
									'taxonomy'         => 'refs_content_category',
									'field'			   => 'slug',
									'terms'            => $atts->group,
								),
							);
	}

	// The Query
	$query = new WP_Query( $args );

	// The Loop
	if ( $query->have_posts() ) {

		$content = '<div '.($atts->carousel_in_mobile?'id="carousel-refsContent" ':'').'class="content-refs '.$atts->class_developer.'" >';
		$i = 1;
		$outline = '';

		while ( $query->have_posts() ) {
			$query->the_post();

			// do something

			switch ($atts->type) {
				case 'line':
					/********* Template LINE **********/

					


					/********* End LINE **********/

					break;



				case 'grid':

					/********* Template GRID **********/
					$is_link = get_post_meta(get_the_ID(), "content_link", true);
					$is_icon = get_post_meta(get_the_ID(), "content_icon", true);

					$outline .= '<div class="item-content slide ' . $class_cols . '" >';

					if( has_post_thumbnail() ){
						$thumbnail = wp_get_attachment_image_url( get_post_thumbnail_id(), 'refs_content_mini' );
						$outline .= '<figure>';
						$outline .= '<img src="'.$thumbnail.'" alt="'.(get_the_title()).'" class="rc-icon-image '.$class_icon.'" />';
						$outline .= '</figure>';
					}elseif( !empty($is_icon) ){
						$outline .= '<div class="rc-icon '.$class_icon.'" >';
						$outline .= '<i class="fa ' . get_post_meta(get_the_ID(), "content_icon", true) . '" ></i>';
						$outline .= '</div>';
					}

					if( !empty($is_link) ){
						$outline .= '<a href="' . get_post_meta(get_the_ID(), "content_link", true) . '" class="rc-link" target="' . $atts->link_target . '" >';
					}

					$outline .= '<'.$atts->tag_title.' class="rc-title '.$class_title.'">';
					$outline .= get_the_title();
					$outline .= '</'.$atts->tag_title.'>';

					$outline .= '<p class="rc-content '.$class_text.'">';
					$outline .= get_the_content();
					$outline .= '</p>';

					if( !empty($is_link) ){
						$outline .= '</a>';
					}

					$outline .= '</div>';


					/*if( is_int($i/$atts->coluns) ){
						$content .= '<div class="row" >' . $outline . '</div>';
						$outline = '';
					}*/

					


					/********* End GRID **********/

					break;
				
				
			}

			$i++;

		}

		/*if($outline != ''){
			$content = '<div class="row" >' . $outline . '</div>';
		}*/
		$content .= $outline;
		$content .= '</div>';

		echo $content;


	} else {
		// no posts found
		echo 'ERRO';
	}

	// Restore original Post Data
	wp_reset_postdata();

	



}
add_shortcode( 'rfs_content', 'refs_content_custom_shortcode' );